# Scale Out Games
A provider of Online Ganes and Casion Software has all of his Infrastructure running inside a Datacenter in Malta.
The enviroment consists of services like:
 - Billing & Customer Database
 - Game Servers
 - Web Front ends
 - Datacenter Management
 - Backups
 - Data Warehouse

Since the Datacenter is almost maxed out and floorspace in malta is extemely expensive, the provider wants to scale out as much load to public clouds as possible. Online Casino regulations and data protection rules require to keep some core services (Userdata, Game Logs, specialised Random Number Generator Hardware) inside the Datacenter.

The Scale out project requires a Dev-Environment in the cloud (with secured connections to the backend), in which Developers can spin Services as needed. In addition to new apps being directly developed in the cloud, the provider wants to migrate existing apps to the cloud. 

For this customer you should propose:
- A self servicable Dev-Environment
- Tools to analyse the datacenter load to calculate ressources that later will be needed in the cloud
- Chargeback Reports for new dev and existing on premise solutions
The Cloud Strategy must not be tied to one vendor, but open to use any clouds.

Describe the Architecture that you'd build for this and outlinethe first steps into the new Environment.


